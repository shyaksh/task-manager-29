package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.ServerInfoEndpoint;

public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String argument() {
        return "-s";
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host and port.";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");
        @NotNull final ServerInfoEndpoint serverInfoEndpoint = endpointLocator.getServerInfoEndpoint();
        System.out.println("Server host: " + serverInfoEndpoint.getServerHost());
        System.out.println("Server port: " + serverInfoEndpoint.getServerPort());
    }

}
