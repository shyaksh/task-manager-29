package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<SessionDTO> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager em) {
        super(em);
    }

    @NotNull
    @Override
    public List<SessionDTO> findByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        em.createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        return em.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findById(@NotNull String id) {
        return em.createQuery("SELECT e FROM SessionDTO e WHERE e.id = :id", SessionDTO.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long count() {
        return em.createQuery("SELECT COUNT(e) FROM SessionDTO e", Long.class).getSingleResult();
    }

    @Override
    public void remove(@NotNull final SessionDTO dto) {
        @Nullable final Session session = em.createQuery("SELECT e FROM Session e WHERE e.id = :id", Session.class)
                .setParameter("id", dto.getId())
                .getSingleResult();
        if (session == null) return;
        em.remove(session);
    }

}
