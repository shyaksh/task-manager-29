package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.AbstractEntityDTO;

import java.util.List;

public interface IRepository<E extends AbstractEntityDTO> {

    void begin();

    void commit();

    void rollback();

    void close();

    @NotNull
    List<E> findAll();

    void persist(@NotNull E e);

    void merge(@NotNull E e);

    void mergeAll(@NotNull List<E> list);

    void remove(@NotNull E e);

    @Nullable
    E findById(@NotNull String id);

    void load(@NotNull List<E> list);

    void clear();

    long count();

    boolean contains(@NotNull E e);

}
